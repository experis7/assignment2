﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Models
{
    public class Customer
    {
        public Customer(string firstName,string lastName,string country, string postalCode,string phoneNumber,string email)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.country = country;
            this.postalCode = postalCode;
            this.phoneNumber = phoneNumber;
            this.email = email;
            
        }
        public Customer()
        {

        }
        public Customer(int Id, string firstName, string lastName, string country, string postalCode, string phoneNumber, string email)
        {
            this.Id = Id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.country = country;
            this.postalCode = postalCode;
            this.phoneNumber = phoneNumber;
            this.email = email;

        }

        //Uses "?" to allow the class to be created if some of the values from the Database is null
        public int Id { get; set; }
        public string firstName { get; set; }
        public string? lastName { get; set; } 
        public string? country { get; set; }
        public string? postalCode { get; set; }
        public string? phoneNumber { get; set; }
        public string? email { get; set; }


    }
}
