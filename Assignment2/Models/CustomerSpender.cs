﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Models
{
    public class CustomerSpender
    {
        public Customer customer { get; set; }
        public decimal amountSpent { get; set; }

        public CustomerSpender()
        {
        }
    }
}
