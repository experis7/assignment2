﻿// See https://aka.ms/new-console-template for more information

using Assignment2.Models;
using Assignment2.Repository;
using System.Diagnostics;

ICustomerRepository repository = new CustomerRepository();
PrintAllCustomers(repository);
GetCustomerByName("Eduardo", repository);
GetCustomerPage(0, repository);
Customer test = new Customer(60, "Test", "ZZZ", "Sweden", "1111", "999", "Sondre.eftedal@no.experis.com");
Console.WriteLine(EditCustomer(test,repository));
PrintCustomersOrderedByAmountSpent(repository);
PrintNumberOfCustomersPerCountry(repository);
PrintMostPopularGenreForCustomer(4, repository);



//No summaries needed for the scaffolding because the methods is private
static void PrintAllCustomers(ICustomerRepository repository)
{
    List<Customer> customerList = repository.GetAllCustomers();
        foreach(var customer in customerList)
        {
        PrintCustomer(customer);
        }
    
}

static void GetCustomerById(int id, ICustomerRepository repository)
{
    Customer customer = repository.GetCustomer(id);
    PrintCustomer(customer);
} 

static void PrintCustomer(Customer customer)
{
    Console.WriteLine($"Customer Id:{customer.Id}, First name: {customer.firstName}, Last Name: {customer.lastName}, Country: {customer.country}, Postal code: {customer.postalCode}, Phone: {customer.phoneNumber}, Email:{customer.email}");
}
static void GetCustomerByName(string name, ICustomerRepository repository)
{
    Customer customer = repository.GetCustomer(name);
    PrintCustomer(customer);
}
static void GetCustomerPage(int pageNumber, ICustomerRepository repository)
{
    List<Customer> customerList = repository.GetCustomerPage(pageNumber);
    foreach(var customer in customerList)
    {
        PrintCustomer(customer);
    }

}

static bool AddCustomer(Customer customer, ICustomerRepository repository)
{
    return repository.AddCustomer(customer);
}
static bool EditCustomer(Customer customer, ICustomerRepository repository)
{
    return repository.UpdateCustomer(customer);
}
static void PrintCustomersOrderedByAmountSpent(ICustomerRepository repository)
{
    List<CustomerSpender> customerList = repository.GetHighestSpendingCustomers();
    foreach(var customer in customerList)
    {
        PrintCustomer(customer.customer);
        Console.WriteLine($"Amount Spent: {customer.amountSpent}");
        Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");

    }
}
static void PrintNumberOfCustomersPerCountry(ICustomerRepository repository)
{
    List<CustomerCountry> listOfCountriesWithNumberOfCustomers = repository.GetNumberOfCustomersInCountries();
    foreach(var country in listOfCountriesWithNumberOfCustomers)
    {
        Console.WriteLine($"Country: {country.country} Number of customers: {country.numberOfCustomers}");
    }
}
static void PrintMostPopularGenreForCustomer(int customerId, ICustomerRepository repository)
{
    List<CustomerGenre> mostPopularGenre = repository.GetMostPopularGenreForCustomer(customerId);
    foreach(var genre in mostPopularGenre)
    {
        Console.WriteLine($"Most popular genre for {genre.customerName} is {genre.mostPopularGenre}!");
    }
}