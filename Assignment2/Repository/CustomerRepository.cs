﻿using Assignment2.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        
        public bool AddCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName,LastName,Country,PostalCode,Phone,Email)" + "VALUES(@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("FirstName", customer.firstName);
                        cmd.Parameters.AddWithValue("LastName", customer.lastName);
                        cmd.Parameters.AddWithValue("Country", customer.country);
                        cmd.Parameters.AddWithValue("PostalCode", customer.postalCode);
                        cmd.Parameters.AddWithValue("Phone", customer.phoneNumber);
                        cmd.Parameters.AddWithValue("Email", customer.email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;                     
                    }


                }


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
    }
        
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                //Make a command
                using(SqlCommand cmd = new SqlCommand(sql, connection))
                    {   
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                 //Handle the result
                                 Customer temp = new Customer();
                                temp.Id = reader.GetInt32(0);
                                temp.firstName = reader.GetString(1);
                                temp.lastName= reader.GetString(2);
                                temp.country = reader.GetString(3);
                                //Handles the null values that may be returned
                                temp.postalCode = reader.IsDBNull(4) ? "no data" : reader.GetString(4);
                                temp.phoneNumber = reader.IsDBNull(5) ? "no data" : reader.GetString(5);

                                temp.email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                

                }

                
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @id";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("id", id);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle the result                            
                                customer.Id = reader.GetInt32(0);
                                customer.firstName = reader.GetString(1);
                                customer.lastName = reader.GetString(2);
                                customer.country = reader.GetString(3);
                                //Handles the null values that may be returned
                                customer.postalCode = reader.IsDBNull(4) ? "no data" : reader.GetString(4);
                                customer.phoneNumber = reader.IsDBNull(5) ? "no data" : reader.GetString(5);

                                customer.email = reader.GetString(6);
                                
                            }
                        }
                    }


                }


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public Customer GetCustomer(string name)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName = @FirstName";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("FirstName", name);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle the result                            
                                customer.Id = reader.GetInt32(0);
                                customer.firstName = reader.GetString(1);
                                customer.lastName = reader.GetString(2);
                                customer.country = reader.GetString(3);
                                //Handles the null values that may be returned
                                customer.postalCode = reader.IsDBNull(4) ? "no data" : reader.GetString(4);
                                customer.phoneNumber = reader.IsDBNull(5) ? "no data" : reader.GetString(5);

                                customer.email = reader.GetString(6);

                            }
                        }
                    }


                }


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public List<Customer> GetCustomerPage(int pageNumber)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT top 10 CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer Where CustomerId > @PageNumber";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //Multiplies pagenumber by 10 because each page is set to have 10 customers.                      
                        cmd.Parameters.AddWithValue("PageNumber", pageNumber  * 10);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle the result
                                Customer temp = new Customer();
                                temp.Id = reader.GetInt32(0);
                                temp.firstName = reader.GetString(1);
                                temp.lastName = reader.GetString(2);
                                temp.country = reader.GetString(3);
                                //Handles the null values that may be returned
                                temp.postalCode = reader.IsDBNull(4) ? "no data" : reader.GetString(4);
                                
                                temp.phoneNumber = reader.IsDBNull(5) ? "no data" : reader.GetString(5);

                                temp.email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }


                }


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        public List<CustomerSpender> GetHighestSpendingCustomers()
        {
            List<CustomerSpender> customersOrderedByTotalSpent = new List<CustomerSpender>();
            string sql = "SELECT Customer.FirstName,Invoice.Total " + "FROM Customer,Invoice " + " WHERE Customer.CustomerId = Invoice.CustomerId " + "ORDER BY Invoice.Total desc";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle the result
                                CustomerSpender temp = new CustomerSpender();
                                temp.customer = GetCustomer(reader.GetString(0));
                                temp.amountSpent = reader.GetDecimal(1);

                                
                               
                                customersOrderedByTotalSpent.Add(temp);
                            }
                        }
                    }


                }


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customersOrderedByTotalSpent;
        }

        public List<CustomerGenre> GetMostPopularGenreForCustomer(int customerId)
        {
            List<CustomerGenre> mostPopularGenre= new List<CustomerGenre>();
            string sql = "Select TOP 1 WITH ties Customer.FirstName, Genre.Name " + "From Customer " + "INNER JOIN  Invoice on Invoice.CustomerId = Customer.CustomerId " + "INNER JOIN InvoiceLine on Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "INNER JOIN Track on Track.TrackId = InvoiceLine.TrackId " + "INNER JOIN Genre on Genre.GenreId = Track.GenreId " + "WHERE Customer.CustomerId = @customerId " + "GROUP BY Customer.FirstName, Genre.Name " + 
                "Order by COUNT(Genre.Name) desc";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("customerId", customerId);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle the result
                                CustomerGenre temp = new CustomerGenre();
                                temp.customerName = reader.GetString(0);
                                temp.mostPopularGenre = reader.GetString(1);



                                mostPopularGenre.Add(temp);
                            }
                        }
                    }


                }


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return mostPopularGenre;
        }

        public List<CustomerCountry> GetNumberOfCustomersInCountries()
        {
            List<CustomerCountry> customersInCountries = new List<CustomerCountry>();
            string sql = "SELECT Country,COUNT(*) " + "From Customer " + "Group by Country";
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {

                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //Handle the result
                                CustomerCountry temp = new CustomerCountry();
                                temp.country = reader.GetString(0);
                                temp.numberOfCustomers = reader.GetInt32(1);



                                customersInCountries.Add(temp);
                            }
                        }
                    }


                }


            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customersInCountries;
        }

        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer " + "SET FirstName = @FirstName, LastName = @LastName,Country = @Country, Phone = @Phone, PostalCode = @PostalCode, Email = @Email " +
                "WHERE  CustomerId = @Id";
               
            try
            {
                using(SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql,connection))
                    {
                        cmd.Parameters.AddWithValue("@Id", customer.Id);
                        cmd.Parameters.AddWithValue("@Country", customer.country);
                        cmd.Parameters.AddWithValue("@FirstName", customer.firstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.lastName);
                        cmd.Parameters.AddWithValue("@Phone", customer.phoneNumber);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.postalCode);
                        cmd.Parameters.AddWithValue("@Email", customer.email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }
    }
}
