﻿using Assignment2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Repository
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// A method that sends a query to the database in order to get all customers from the customer table 
        /// </summary>
        /// <returns>List of customers</returns>
        public List<Customer> GetAllCustomers();

        /// <summary>
        /// This method takes in an id and sends a query to the database to find the customer with a matching Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer</returns>

        public Customer GetCustomer(int id);

        /// <summary>
        /// This method takes in a string and sends a query to the database to find the customer with a matching name
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer</returns>
        public Customer GetCustomer(string name);
        /// <summary>
        /// Takes in a page number and returns a page of customers, which by default is set to 10.
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns>List of 10 customers</returns>

        public List<Customer> GetCustomerPage(int pageNumber);
        /// <summary>
        /// A method that takes in a customer Object and sends a sql statement to the database in order to add the customer to the database.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Bool: False if there was a problem with the statement and true if the sql was execuded correctly.</returns>
        public bool AddCustomer(Customer customer);
        /// <summary>
        /// Takes in a Customer and sends a statement to the database in order to Update the given customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Bool: True if the update was successful and false if it failed</returns>

        public bool UpdateCustomer(Customer customer);
        /// <summary>
        /// Sends a query to the database to get a list of customers per country.
        /// </summary>
        /// <returns>List of customers in each country</returns>

        public List<CustomerCountry> GetNumberOfCustomersInCountries();
        /// <summary>
        /// Sends a query to the database to find the highest spending customers in decending order.
        /// </summary>
        /// <returns>List of highest spending customers in decending order</returns>
        public List<CustomerSpender> GetHighestSpendingCustomers();
        /// <summary>
        /// Sends a query to the database to find the most popular genre for a given customer with the Id from the parameter.
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>List of the most popular genre for a given customer.</returns>

        public List<CustomerGenre> GetMostPopularGenreForCustomer(int customerId);


    }
}
