# Assignment 2
This assignment consists of two parts. In part 1 we created 9 SQl scripts to perform different tasks to create and retrieve data to a new database. In part 2, we created a console application to perform CRUD operations to a given database.

## Part 1
We created 9 sql scripts to perform the given tasks. Each script can be found in the SQLScripts folder.
## Part 2
The console application project contains Models for the data, methods to connect to the database and an interface with the methods that we later implemented in the repository. We used pair programming to make the application.


