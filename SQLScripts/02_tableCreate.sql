use SuperheroesDb
create table superhero(
Id int identity(1,1) primary key,
Name nvarchar(50),
Alias nvarchar(50),
Origin nvarchar(50)

);

create table assistant(
Id int identity(1,1) primary key,
Name nvarchar(50)
);

create table powers(
Id int identity(1,1) primary key,
Name nvarchar(50),
Description nvarchar(50)
);